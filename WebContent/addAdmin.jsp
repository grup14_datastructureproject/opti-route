<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<title>Add Admin Page</title>
<link href="css/bootstrap.css" rel="stylesheet" />
</head>
<body>
	<div class="col-sm-6">
	<a href="http://localhost:8080/Opti-Route/logout.jsp"> Logout</a>
		<h1 class="label label-default">Add Admin</h1>
		<div class="container-fluid">
			<div class="row">

				<form id="form2" name="form2" method="post" action="addAdminServlet">
					<table class="table">

						<tr>
							<th><label class="label label-default" for="first">Company username
							</label></th>
							<th><input class="form-control col-sm-8" type="text"
								name="companyUsername" id="first" /></th>
						</tr>
						<tr>
							<th><label class="label label-default" for="last">Create Password</label></th>
							<th><input class="form-control col-sm-8" type="text"
								name="createPassword" id="last" /></th>
						</tr>
						<tr>
							<th><label class="label label-default" for="id">Company Name </label></th>
							<th><input class="form-control col-sm-8" type="text"
								name="companyName" id="id" /></th>
						</tr>

						<tr>
							<th></th>
							<th><input class="btn btn-success" type="submit"
								name="gonder" id="gonder" value="KAYIT OL" /></th>
						</tr>

					</table>
				</form>
			</div>
		</div>
	</div>

</body>
</html>