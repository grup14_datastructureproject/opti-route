<%@ page import="java.sql.*"%>
<%@ page import="java.io.*"%>
<%@ page import="java.util.*"%>
<%@ page import="dao.*"%>
<%@ page import="Trip.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<style>
/*the container must be positioned relative:*/
.custom-select {
	position: relative;
	font-family: Arial;
}

.custom-select select {
	display: none; /*hide original SELECT element:*/
}

.select-selected {
	background-color: #ff471a;
}
/*style the arrow inside the select element:*/
.select-selected:after {
	position: absolute;
	content: "";
	top: 14px;
	right: 10px;
	width: 0;
	height: 0;
	border: 6px solid transparent;
	border-color: #fff transparent transparent transparent;
}
/*point the arrow upwards when the select box is open (active):*/
.select-selected.select-arrow-active:after {
	border-color: transparent transparent #fff transparent;
	top: 7px;
}
/*style the items (options), including the selected item:*/
.select-items div, .select-selected {
	color: #ffffff;
	padding: 8px 16px;
	border: 1px solid transparent;
	border-color: transparent transparent rgba(0, 0, 0, 0.1) transparent;
	cursor: pointer;
	user-select: none;
}
/*style items (options):*/
.select-items {
	position: absolute;
	background-color: #ff471a;
	top: 100%;
	left: 0;
	right: 0;
	z-index: 99;
}
/*hide the items when the select box is closed:*/
.select-hide {
	display: none;
}

.select-items div:hover, .same-as-selected {
	background-color: rgba(0, 0, 0, 0.1);
}

div.ex1 {
	background-color: #ffffff;
	width: 750px;
	height: 350px;
	overflow: scroll;
	background-color: #ebebe0
	
}
</style>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

	<center>
		<table width="800" height="600" bgcolor="#e6e6ff">

			<tr>
				<td>
					<table>
						<tr width="800" height="40" bgcolor="#ff471a">

							<td width="80">
								<center>
									<a href="home.jsp"> <img src="images/home_icon.png"
										width="30" height="30">
									</a>
								</center>
							</td>

							<td width="180">
								<center>
									<table>
										<tr>
											<td width="40">
												<center>
													<img src="images/email_icon.png" width="20" height="18">
												</center>
											</td>

											<td width="140"><strong> CONTACT US</strong></td>
										</tr>
									</table>


								</center>
							</td>

							<td width="400">
								<center>
									<font size="6"> <strong> OPTI-ROUTE </strong>
									</font>
								</center>

							</td>

							<td width="100" bgcolor="#ebebe0">

								<center>
									<img src="images/search_icon.png" width="20" height="20">
								</center>
							</td>

							<td width="80">
								<center></center>
							</td>

						</tr>

					</table>

					<table width="800" height="100">

						<tr>

							<td width="50"></td>

							<td width="350"><img
								src="images/secondpage_icon.png" width="300" height="300"></td>


							<td>
								<div class="custom-select">
									<select>
										<option value="0">Sort By:</option>
										<option value="1">Best</option>
										<option value="2">Cheapest First</option>
										<option value="3">Fastest First</option>
										<option value="4">Departure Time</option>

									</select>
								</div>
							</td>
						</tr>

					</table> 
					<br><br><br>
					<center>
					<div class="ex1"> 
					<table width="800" height="500"   border="1">
					
							<center>
									<%
										ApplicationDAOImpl dao = new ApplicationDAOImpl();
										String from = request.getParameter("firstname");
										String to = request.getParameter("lastname");
										System.out.println("aaaaJSPPPPPP " + from + to);
										Trip trip = new Trip(from,to," ", " ");

										List<Trip> array = dao.selectTrip(trip);
									%>
									

										<tr>

											<td><%="COMPANY"%></td>
											<td><%="FROM"%></td>
											<td><%="WHERE"%></td>
											<td><%="DTIME"%></td>
											<td><%="ATIME"%></td>
											<td><%="PRICE"%></td>
										</tr>
										
									
										
										<tr style="border: 0pt" >
											<td><strong><%="\nHighway\n"%></strong></td>
										</tr>
										
										
										
										<%
											for (Trip t : array) {
												if (t.getCompanyType().equals("0")) { //KAra
										%>
										<tr>
											
											<td><%=t.getCompanyName()%></td>
											<td><%=t.getFromCountry()%></td>
											<td><%=t.getWhereCountry()%></td>
											<td><%=t.getDepartureTime()%></td>
											<td><%=t.getArrivalTime()%></td>
											<td><%=t.getPrice()%></td>
										</tr>
										<%
											}

											}
										%>
										
									

										<tr style="border: 0pt" >
											<td><strong><%="\nRailway\n"%></strong></td>
										</tr>
										
										
										
										<%
											for (Trip t : array) {
												if (t.getCompanyType().equals("1")) { //Demir
										%>
										<tr>

											<td><%=t.getCompanyName()%></td>
											<td><%=t.getFromCountry()%></td>
											<td><%=t.getWhereCountry()%></td>
											<td><%=t.getDepartureTime()%></td>
											<td><%=t.getArrivalTime()%></td>
											<td><%=t.getPrice()%></td>
										</tr>
										<%
											}

											}
										%>
										
										
										
										<tr style="border: 0pt" >
											<td><strong><%="\nAirway\n"%></strong></td>
										</tr>
									
									
										
										<%
											for (Trip t : array) {
												if (t.getCompanyType().equals("2")) { //Hava
										%>
										<tr>

											<td><%=t.getCompanyName()%></td>
											<td><%=t.getFromCountry()%></td>
											<td><%=t.getWhereCountry()%></td>
											<td><%=t.getDepartureTime()%></td>
											<td><%=t.getArrivalTime()%></td>
											<td><%=t.getPrice()%></td>
										</tr>
										<%
											}

											}
										%>
										

										<tr style="border: 0pt" >
											<td><strong><%="\nSeaway\n"%></strong></td>
										</tr>
										
									
										
										<%
											for (Trip t : array) {
												if (t.getCompanyType().equals("3")) { //Deniz
										%>
										<tr>

											<td><%=t.getCompanyName()%></td>
											<td><%=t.getFromCountry()%></td>
											<td><%=t.getWhereCountry()%></td>
											<td><%=t.getDepartureTime()%></td>
											<td><%=t.getArrivalTime()%></td>
											<td><%=t.getPrice()%></td>
										</tr>
										<%
											}

											}
										%>

							</center>
						
					</table>
					</div>
					</center>
					</td>
			</tr>
		</table>


		<script>
			var x, i, j, selElmnt, a, b, c;
			/*look for any elements with the class "custom-select":*/
			x = document.getElementsByClassName("custom-select");
			for (i = 0; i < x.length; i++) {
				selElmnt = x[i].getElementsByTagName("select")[0];
				/*for each element, create a new DIV that will act as the selected item:*/
				a = document.createElement("DIV");
				a.setAttribute("class", "select-selected");
				a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
				x[i].appendChild(a);
				/*for each element, create a new DIV that will contain the option list:*/
				b = document.createElement("DIV");
				b.setAttribute("class", "select-items select-hide");
				for (j = 0; j < selElmnt.length; j++) {
					/*for each option in the original select element,
					create a new DIV that will act as an option item:*/
					c = document.createElement("DIV");
					c.innerHTML = selElmnt.options[j].innerHTML;
					c
							.addEventListener(
									"click",
									function(e) {
										/*when an item is clicked, update the original select box,
										and the selected item:*/
										var y, i, k, s, h;
										s = this.parentNode.parentNode
												.getElementsByTagName("select")[0];
										h = this.parentNode.previousSibling;
										for (i = 0; i < s.length; i++) {
											if (s.options[i].innerHTML == this.innerHTML) {
												s.selectedIndex = i;
												h.innerHTML = this.innerHTML;
												y = this.parentNode
														.getElementsByClassName("same-as-selected");
												for (k = 0; k < y.length; k++) {
													y[k]
															.removeAttribute("class");
												}
												this.setAttribute("class",
														"same-as-selected");
												break;
											}
										}
										h.click();
									});
					b.appendChild(c);
				}
				x[i].appendChild(b);
				a.addEventListener("click", function(e) {
					/*when the select box is clicked, close any other select boxes,
					and open/close the current select box:*/
					e.stopPropagation();
					closeAllSelect(this);
					this.nextSibling.classList.toggle("select-hide");
					this.classList.toggle("select-arrow-active");
				});
			}
			function closeAllSelect(elmnt) {
				/*a function that will close all select boxes in the document,
				except the current select box:*/
				var x, y, i, arrNo = [];
				x = document.getElementsByClassName("select-items");
				y = document.getElementsByClassName("select-selected");
				for (i = 0; i < y.length; i++) {
					if (elmnt == y[i]) {
						arrNo.push(i)
					} else {
						y[i].classList.remove("select-arrow-active");
					}
				}
				for (i = 0; i < x.length; i++) {
					if (arrNo.indexOf(i)) {
						x[i].classList.add("select-hide");
					}
				}
			}
			/*if the user clicks anywhere outside the select box,
			then close all select boxes:*/
			document.addEventListener("click", closeAllSelect);
		</script>
	</center>


</body>
</html>