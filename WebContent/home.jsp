<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<html class="no-js">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href="css/bootstrap.css" rel="stylesheet" />
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Travel &mdash; 100% Free Fully Responsive HTML5 Template
	by FREEHTML5.co</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Free HTML5 Template by FREEHTML5.CO" />
<meta name="keywords"
	content="free html5, free template, free bootstrap, html5, css3, mobile first, responsive" />
<meta name="author" content="FREEHTML5.CO" />

<!-- 
	//////////////////////////////////////////////////////

	FREE HTML5 TEMPLATE 
	DESIGNED & DEVELOPED by FREEHTML5.CO
		
	Website: 		http://freehtml5.co/
	Email: 			info@freehtml5.co
	Twitter: 		http://twitter.com/fh5co
	Facebook: 		https://www.facebook.com/fh5co

	//////////////////////////////////////////////////////
	 -->

<!-- Facebook and Twitter integration -->
<meta property="og:title" content="" />
<meta property="og:image" content="" />
<meta property="og:url" content="" />
<meta property="og:site_name" content="" />
<meta property="og:description" content="" />
<meta name="twitter:title" content="" />
<meta name="twitter:image" content="" />
<meta name="twitter:url" content="" />
<meta name="twitter:card" content="" />

<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<link rel="shortcut icon" href="favicon.ico">

<link
	href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,300'
	rel='stylesheet' type='text/css'>

<!-- Animate.css -->
<link rel="stylesheet" href="css/animate.css">
<!-- Icomoon Icon Fonts-->
<link rel="stylesheet" href="css/icomoon.css">
<!-- Bootstrap  -->
<link rel="stylesheet" href="css/bootstrap.css">
<!-- Superfish -->
<link rel="stylesheet" href="css/superfish.css">
<!-- Magnific Popup -->
<link rel="stylesheet" href="css/magnific-popup.css">
<!-- Date Picker -->
<link rel="stylesheet" href="css/bootstrap-datepicker.min.css">
<!-- CS Select -->
<link rel="stylesheet" href="css/cs-select.css">
<link rel="stylesheet" href="css/cs-skin-border.css">

<link rel="stylesheet" href="css/style.css">


<!-- Modernizr JS -->
<script src="js/modernizr-2.6.2.min.js"></script>
</head>
 <style>
input[type=text], select {
	width: 80%;
	padding: 12px 20px;
	margin: 8px 0;
	display: inline-block;
	border: 2px solid #ccc;
	border-radius: 4px;
	box-sizing: border-box;
}

input[type=date], select {
	width: 80%;
	padding: 12px 20px;
	margin: 8px 0;
	display: inline-block;
	border: 2px solid #ccc;
	border-radius: 4px;
	box-sizing: border-box;
}

input[type=submit], select {
	width: 80%;
	padding: 12px 20px;
	margin: 8px 0;
	display: inline-block;
	border: 2px solid #ccc;
	border-radius: 4px;
	box-sizing: border-box;
	background-color: #ff471a;
	font-weight: bold;
	font-size: 20;
}

a.button {
	text-decoration: none;
	padding: 8px;
	display: block;
	font-size: 20px;
	border-radius: 7px;
	background-color: #ff471a;
	color: #fff;
	background-image: linear-gradient(to bottom, #ff471a, #e6e6ff);
	width: 290px;
	height: 30px;
	margin-left: 5px;
}

a:hover {
	background-color: gray;
	color: #e6e6ff;
	background-image: none;
}

a:active {
	background-color: #ff471a;
	color: #fff;
	background-image: none;
	border-color: rgba(0, 0, 0, .2);
	border-top-color: rgba(0, 0, 0, .4);
	box-shadow: inset 0 2px 4px rgba(0, 0, 0, .2);
}
</style>
<body>
	<div id="fh5co-wrapper">
		<div id="fh5co-page">

			<header id="fh5co-header-section" class="sticky-banner">
			<div class="container">
				<div class="nav-header">
					<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle dark"><i></i></a>
					<h1 id="fh5co-logo">
						<a href="index.html"><i class="icon-airplane"></i>Opti-Route</a>
					</h1>
					<!-- START #fh5co-menu-wrap -->
					<nav id="fh5co-menu-wrap" role="navigation">
					<ul class="sf-menu" id="fh5co-primary-menu">
						<li class="active"><a href="index.html">Home</a></li>
						<li><a href="blog.html">Sign in</a></li>
						<li><a href="contact.html">Contact</a></li>
					</ul>
					</nav>
				</div>
			</div>
			</header>
			<center>
				<table width="800" height="600" bgcolor="#e6e6ff">

					<tr>
						<td>

							<table>
								<tr width="800" height="40" bgcolor="#ff471a">

									<td width="80">
										<center>

											<img src="images/home_icon.png" width="30" height="30">

										</center>
									</td>

									<td width="180">
										<center>
											<table>
												<tr>
													<td width="40">
														<center>
															<img src="images/email_icon.png" width="20" height="18">
														</center>
													</td>

													<td width="140"><strong> CONTACT US</strong></td>
												</tr>
											</table>


										</center>
									</td>

									<td width="400">
										<center>
											<font size="6"> <strong> OPTI-ROUTE </strong>
											</font>
										</center>

									</td>

									<td width="100" bgcolor="#ebebe0">
										<center>
											<img src="images/search_icon.png" width="20" height="20">
										</center>
									</td>

									<td width="80">
										<center>
											<a href="login.jsp">

												<p>Sign In</p>


											</a>

										</center>
									</td>

								</tr>


							</table> <br> <br>

							<table width="800" height="400">
								<tr>
									
									<td width="400" height="400">

										<form id="form2" name="form2" action="HomeServlet"
											method="post">
											<input type="text" name="firstname" placeholder="From">
											<input type="text" name="lastname" placeholder="To">
											<input type="date" name="bday" max="2020-04-25"> <input
												type="date" name="bday" min="2018-04-25"><br> <br>
											<input class="btn btn-success" type="submit" name="gonder"
												id="gonder" value="Search Route" />

										</form>


									</td>

									
									<td width="400" height="400">
										<center>
											<img src="images/firstpage_icon.png" width="340" height="340">
										</center>
									</td>
								</tr>
							</table>

						</td>
					</tr>
				</table>
			</center>

			<div class="fh5co-hero">
				<div class="fh5co-overlay"></div>
				<div class="fh5co-cover" data-stellar-background-ratio="0.5"
					style="background-image: url(images/cover_bg_1.jpg);">
					<div class="desc">
						<div class="container">
							<div class="row">
								<div class="col-sm-5 col-md-5">
									<div class="tabulation animate-box">

										<!-- Nav tabs -->
										<ul class="nav nav-tabs" role="tablist">
											<li role="presentation" class="active"><a
												href="#flights" aria-controls="flights" role="tab"
												data-toggle="tab">Travels</a></li>

										</ul>

										<!-- Tab panes -->
										<div class="tab-content">
											<div role="tabpanel" class="tab-pane active" id="flights">
												<div class="row">
													<div class="col-xxs-12 col-xs-6 mt">
														<div class="input-field">
															<label for="from">From:</label> <input type="text"
																class="form-control" id="from-place"
																placeholder="Los Angeles, USA" />
														</div>
													</div>
													<div class="col-xxs-12 col-xs-6 mt">
														<div class="input-field">
															<label for="from">To:</label> <input type="text"
																class="form-control" id="to-place"
																placeholder="Tokyo, Japan" />
														</div>
													</div>
													<div class="col-xxs-12 col-xs-6 mt alternate">
														<div class="input-field">
															<label for="date-start">Check In:</label> <input
																type="text" class="form-control" id="date-start"
																placeholder="mm/dd/yyyy" />
														</div>
													</div>
													<div class="col-xxs-12 col-xs-6 mt alternate">
														<div class="input-field">
															<label for="date-end">Check Out:</label> <input
																type="text" class="form-control" id="date-end"
																placeholder="mm/dd/yyyy" />
														</div>
													</div>
													<div class="col-xs-12">
														<input type="submit" class="btn btn-primary btn-block"
															value="Search Route">
													</div>
												</div>
											</div>
										</div>

									</div>
								</div>
								<div class="desc2 animate-box">
									<div class="col-sm-7 col-sm-push-1 col-md-7 col-md-push-1">

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>



			<div id="fh5co-tours" class="fh5co-section-gray">
				<div class="container">
					<div class="row">
						<div
							class="col-md-8 col-md-offset-2 text-center heading-section animate-box">
							<h3>Hot Tours</h3>
							<p>Far far away, behind the word mountains, far from the
								countries Vokalia and Consonantia, there live the blind texts.</p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4 col-sm-6 fh5co-tours animate-box"
							data-animate-effect="fadeIn">
							<div href="#">
								<img src="images/place-1.jpg"
									alt="Free HTML5 Website Template by FreeHTML5.co"
									class="img-responsive">
								<div class="desc">
									<span></span>
									<h3>New York</h3>
									<span>3 nights + Flight 5*Hotel</span> <span class="price">$1,000</span>
									<a class="btn btn-primary btn-outline" href="#">Book Now <i
										class="icon-arrow-right22"></i></a>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 fh5co-tours animate-box"
							data-animate-effect="fadeIn">
							<div href="#">
								<img src="images/place-2.jpg"
									alt="Free HTML5 Website Template by FreeHTML5.co"
									class="img-responsive">
								<div class="desc">
									<span></span>
									<h3>Philippines</h3>
									<span>4 nights + Flight 5*Hotel</span> <span class="price">$1,000</span>
									<a class="btn btn-primary btn-outline" href="#">Book Now <i
										class="icon-arrow-right22"></i></a>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 fh5co-tours animate-box"
							data-animate-effect="fadeIn">
							<div href="#">
								<img src="images/place-3.jpg"
									alt="Free HTML5 Website Template by FreeHTML5.co"
									class="img-responsive">
								<div class="desc">
									<span></span>
									<h3>Hongkong</h3>
									<span>2 nights + Flight 4*Hotel</span> <span class="price">$1,000</span>
									<a class="btn btn-primary btn-outline" href="#">Book Now <i
										class="icon-arrow-right22"></i></a>
								</div>
							</div>
						</div>
						<div class="col-md-12 text-center animate-box">
							<p>
								<a class="btn btn-primary btn-outline btn-lg" href="#">See
									All Offers <i class="icon-arrow-right22"></i>
								</a>
							</p>
						</div>
					</div>
				</div>
			</div>

			<div id="fh5co-features">
				<div class="container">
					<div class="row">
						<div class="col-md-4 animate-box">

							<div class="feature-left">
								<span class="icon"> <i class="icon-hotairballoon"></i>
								</span>
								<div class="feature-copy">
									<h3>Family Travel</h3>
									<p>Facilis ipsum reprehenderit nemo molestias. Aut cum
										mollitia reprehenderit.</p>
									<p>
										<a href="#">Learn More</a>
									</p>
								</div>
							</div>

						</div>

						<div class="col-md-4 animate-box">
							<div class="feature-left">
								<span class="icon"> <i class="icon-search"></i>
								</span>
								<div class="feature-copy">
									<h3>Travel Plans</h3>
									<p>Facilis ipsum reprehenderit nemo molestias. Aut cum
										mollitia reprehenderit.</p>
									<p>
										<a href="#">Learn More</a>
									</p>
								</div>
							</div>
						</div>
						<div class="col-md-4 animate-box">
							<div class="feature-left">
								<span class="icon"> <i class="icon-wallet"></i>
								</span>
								<div class="feature-copy">
									<h3>Honeymoon</h3>
									<p>Facilis ipsum reprehenderit nemo molestias. Aut cum
										mollitia reprehenderit.</p>
									<p>
										<a href="#">Learn More</a>
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4 animate-box">

							<div class="feature-left">
								<span class="icon"> <i class="icon-wine"></i>
								</span>
								<div class="feature-copy">
									<h3>Business Travel</h3>
									<p>Facilis ipsum reprehenderit nemo molestias. Aut cum
										mollitia reprehenderit.</p>
									<p>
										<a href="#">Learn More</a>
									</p>
								</div>
							</div>

						</div>

						<div class="col-md-4 animate-box">
							<div class="feature-left">
								<span class="icon"> <i class="icon-genius"></i>
								</span>
								<div class="feature-copy">
									<h3>Solo Travel</h3>
									<p>Facilis ipsum reprehenderit nemo molestias. Aut cum
										mollitia reprehenderit.</p>
									<p>
										<a href="#">Learn More</a>
									</p>
								</div>
							</div>

						</div>
						<div class="col-md-4 animate-box">
							<div class="feature-left">
								<span class="icon"> <i class="icon-chat"></i>
								</span>
								<div class="feature-copy">
									<h3>Explorer</h3>
									<p>Facilis ipsum reprehenderit nemo molestias. Aut cum
										mollitia reprehenderit.</p>
									<p>
										<a href="#">Learn More</a>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<footer>
			<div id="footer">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-md-offset-3 text-center">
							<i class="fa fa-love">Opti-Route</a>
						</div>
					</div>
				</div>
			</div>
			</footer>
		</div>
		<!-- END fh5co-page -->

	</div>
	<!-- END fh5co-wrapper -->

	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<script src="js/sticky.js"></script>

	<!-- Stellar -->
	<script src="js/jquery.stellar.min.js"></script>
	<!-- Superfish -->
	<script src="js/hoverIntent.js"></script>
	<script src="js/superfish.js"></script>
	<!-- Magnific Popup -->
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/magnific-popup-options.js"></script>
	<!-- Date Picker -->
	<script src="js/bootstrap-datepicker.min.js"></script>
	<!-- CS Select -->
	<script src="js/classie.js"></script>
	<script src="js/selectFx.js"></script>

	<!-- Main JS -->
	<script src="js/main.js"></script>

</body>
</html>

