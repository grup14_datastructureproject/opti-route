package user;

public class Admin {
	private String username;
	private String password;
	private String company;
	
	public Admin(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}
	public Admin(String username, String password,String company) {
		super();
		this.username = username;
		this.password = password;
		this.company = company;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
}
