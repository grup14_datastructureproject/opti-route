

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebListener;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Trip.Trip;



/**
 * Servlet implementation class StudyServlet
 */
@WebServlet(name = "HomeServlet", urlPatterns = { "/HomeServlet" })
@WebListener
public class HomeServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher rd = req.getRequestDispatcher("home.jsp");
		try{
			
			doPost(req, resp);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
	}

	private static final long serialVersionUID = 1L;

	public HomeServlet() {
		super();

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		try {
			
			String firstname = request.getParameter("firstname");
			String lastname = request.getParameter("lastname");
			


			Trip trip = new Trip(firstname, lastname,"","");


		} catch (Exception e) {
			System.out.println("2. sayfaya geçiş yapamadınız");

		}
		

		RequestDispatcher rd = request.getRequestDispatcher("secondPage.jsp");

		rd.forward(request, response);
	}

}
