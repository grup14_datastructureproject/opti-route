package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import Trip.*;
import user.Admin;


public class ApplicationDAOImpl {

	
	public void create(Admin admin) throws Exception {

		String sql = "INSERT INTO Admin(username,password, company) VALUES (?,?,?)";
		Connection conn = getConnection();
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(sql);
	

		try {
			
			ps.setString(1,admin.getUsername());
			ps.setString(2,admin.getPassword());
			ps.setString(3,admin.getCompany());
		
			System.out.println("Kaydınız Veritabanımıza yapıldı");
			ps.executeUpdate();
			
		} catch (Exception e) {
			System.out.println("Girilen kimlik numarası veritabanında kayıtlıdır.");

		}

		ps.close();
		conn.close();

	}
	public void create(Trip trip) throws Exception {

		String sql = "INSERT INTO Trip(companyType,companyName,fromCountry,whereCountry,departureTime, arrivalTime,price) VALUES (?,?,?,?,?,?,?)";
		Connection conn = getConnection();
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(sql);
		
		try {
			

			ps.setString(1,trip.getCompanyType());
			ps.setString(2,trip.getCompanyName());
			ps.setString(3,trip.getFromCountry());
			ps.setString(4,trip.getWhereCountry());
			ps.setString(5,trip.getDepartureTime());
			ps.setString(6,trip.getArrivalTime());
			ps.setString(7,trip.getPrice());
			
		
			System.out.println("Kaydınız Veritabanımıza yapıldı");
			ps.executeUpdate();
			
		} catch (Exception e) {
			System.out.println("Girilen kimlik numarası veritabanında kayıtlıdır.");

		}

		ps.close();
		conn.close();

	}
	
	public ResultSet selectOne(Admin admin) throws Exception {

		//String sql = "select username,password from Admin where username=? and password=?";
		Connection con = getConnection();
		Statement stmt = con.createStatement();
	    ResultSet rs = stmt.executeQuery("select username,password from Admin where username='"+admin.getUsername()+"' and password='"+admin.getPassword()+"'");
	    
		return rs;
	}
	public ResultSet selectAdmin(Admin admin) throws Exception {

		//String sql = "select * from Admin where username=?";
		Connection con = getConnection();
		Statement stmt = con.createStatement();
	    ResultSet rs = stmt.executeQuery("select * from Admin where username='"+admin.getUsername()+"'");
	    
	    System.out.println("++++++++++++++++++++++++++++++--->" + admin.getCompany());
		return rs;
	}


	public Connection getConnection() throws Exception {
		
		

		try {
			String driver = "com.mysql.jdbc.Driver";
			String url = "jdbc:mysql://localhost:3306/new_schema2?useUnicode=yes&characterEncoding=UTF-8&autoReconnect=true&useSSL=false&maxReconnects=1500";
			String username = "root";
			String password = "30921447";
			Class.forName(driver);

			Connection conn = DriverManager.getConnection(url, username, password);

			System.out.println("Connected");

			return conn;
		} catch (Exception e) {
			System.out.print(e);
			return null;
		}

	}
	public int selectAll(Admin newAdmin) throws Exception {
		
		Connection con = getConnection();
		List<Admin> array = new ArrayList<>();
		try {

			PreparedStatement get = (PreparedStatement) con.prepareStatement("select * from Admin where username='"+newAdmin.getUsername()+"' and password='"+newAdmin.getPassword()+"'");
			ResultSet result = get.executeQuery();

			while (result.next()) {
				// BURASI DEĞİŞECEK
				String username = result.getString("username");
				String password = result.getString("password");
				String company = result.getString("company");
				
				
				
				Admin admin = new Admin(username,password,company);
				array.add(admin);

			}
			System.out.println("array boyutu " + array.size());

		} catch (Exception e) {
			System.out.println("SelectAll " + e);

		} finally {
			con.close();
		}
		return array.size();

	}
	/*public int selectAllTrip(Trip newTrip) throws Exception {
		
		Connection con = getConnection();
		List<Trip> array = new ArrayList<>();
		try {

			PreparedStatement get = (PreparedStatement) con.prepareStatement("select username,password from Trip where username='"+newAdmin.getUsername()+"' and password='"+newAdmin.getPassword()+"'");
			ResultSet result = get.executeQuery();

			while (result.next()) {
				// BURASI DEĞİŞECEK
				String username = result.getString("username");
				String password = result.getString("password");
				String company = result.getString("company");
				
				
				
				Trip trip = new Trip(username,password,company);
				array.add(admin);

			}
			System.out.println("array boyutu " + array.size());

		} catch (Exception e) {
			System.out.println(e);

		} finally {
			con.close();
		}
		return array.size();

	}*/
	
	public List<Trip> selectTrip(Trip trip) throws Exception {
		System.out.println(trip.getFromCountry()+"   Istaaaaanbul");
		Connection con = getConnection();
		List<Trip> array = new ArrayList<Trip>();
		try {

			PreparedStatement get = (PreparedStatement) con.prepareStatement("SELECT * FROM Trip where fromCountry='"+trip.getFromCountry()+"' and whereCountry='"+trip.getWhereCountry()+"'");
			ResultSet result = get.executeQuery();
			
			while (result.next()) {
				// BURASI DEĞİŞECEK
				String companyType = result.getString("companyType");
				String companyName = result.getString("companyName");
				String departureTime = result.getString("departureTime");
				String arrivalTime = result.getString("arrivalTime");
				String price = result.getString("price");
				/*
				long id = result.getLong("id");
				String first = result.getString("first");
				String last = result.getString("last");*/

				//Person person = new Person(id, first, last);
				trip = new Trip(companyType,companyName, trip.getFromCountry(), trip.getWhereCountry(), departureTime , arrivalTime, price);
				array.add(trip);

			}
			
			System.out.println("All records have been selected Trip");

		} catch (Exception e) {
			System.out.println(e);

		} finally {
			con.close();
		}
		return array;

	}

}

