

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Trip.Trip;
import dao.ApplicationDAOImpl;
import user.Admin;

/**
 * Servlet implementation class addAdminServlet
 */
@WebServlet("/addAdminServlet")
public class addAdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public addAdminServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		try{
			// TODO Auto-generated method stub
			System.out.println("doGet metodu");
			// response.getWriter().append("Served at:
			// ").append(request.getContextPath());
			response.sendRedirect("login.jsp");
			doPost(request, response);
		}
		catch(IOException e){
			e.printStackTrace();
		}
		catch(ServletException e){
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		PrintWriter out = response.getWriter();
		System.out.println("doPost metodu");

		String companyUsername = request.getParameter("companyUsername");
		String createPassword = request.getParameter("createPassword");
		String companyName = request.getParameter("companyName");

		System.out.println("-------------------------> " + companyUsername);
		Admin admin = new Admin(companyUsername, createPassword,companyName);

		
		ApplicationDAOImpl dao = new ApplicationDAOImpl();
		
		try {
			int columnsNumber = dao.selectAll(admin);
			ResultSet rs = dao.selectAdmin(admin);
			
			if (columnsNumber ==0) {
				HttpSession session = request.getSession();
				session.setAttribute("name", companyUsername);
				
				dao.create(admin);
				response.sendRedirect("Record.jsp?");
				
					

			} else {
				response.sendRedirect("Error.jsp");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

}
