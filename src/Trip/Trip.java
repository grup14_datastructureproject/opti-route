package Trip;


public class Trip {
	
	String companyType ;
	String companyName = "";
	String fromCountry = "";
	String whereCountry = "";
	String departureTime ;
	String arrivalTime ;
	String price ;
	
	public Trip(String companyType,String companyName, String fromCountry, String whereCountry, String departureTime, String arrivalTime,
			String price) {
		this.companyType = companyType;
		this.companyName = companyName;
		this.fromCountry = fromCountry;
		this.whereCountry = whereCountry;
		this.departureTime = departureTime;
		this.arrivalTime = arrivalTime;
		this.price = price;
	}
	public Trip(String fromCountry, String whereCountry, String departureTime, String arrivalTime) {
		this.fromCountry = fromCountry;
		this.whereCountry = whereCountry;
		this.departureTime = departureTime;
		this.arrivalTime = arrivalTime;
	}
	public String getCompanyType() {
		return companyType;
	}
	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getFromCountry() {
		return fromCountry;
	}
	public void setFromCountry(String fromCountry) {
		this.fromCountry = fromCountry;
	}
	public String getWhereCountry() {
		return whereCountry;
	}
	public void setWhereCountry(String whereCountry) {
		this.whereCountry = whereCountry;
	}
	public String getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}
	public String getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	
	
	

}
