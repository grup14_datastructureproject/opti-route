

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Trip.Trip;
import dao.ApplicationDAOImpl;
import user.Admin;

/**
 * Servlet implementation class addTripServlet
 */
@WebServlet("/addTripServlet")
public class addTripServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public addTripServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			// TODO Auto-generated method stub
			System.out.println("doGet metodu");
			// response.getWriter().append("Served at:
			// ").append(request.getContextPath());
			response.sendRedirect("login.jsp");
			doPost(request, response);
		}
		catch(IOException e){
			e.printStackTrace();
		}
		catch(ServletException e){
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		System.out.println("doPost metodu");

		String companyType = request.getParameter("companyType");
		String companyName = request.getParameter("companyName");
		String fromCountry = request.getParameter("fromCountry");
		String whereCountry = request.getParameter("whereCountry");
		String departureTime = request.getParameter("departureTime");
		String arrivalTime = request.getParameter("arrivalTime");
		String price = request.getParameter("price");

		Trip trip = new Trip(companyType,companyName,fromCountry,whereCountry,departureTime,arrivalTime,price);
		
		ApplicationDAOImpl dao = new ApplicationDAOImpl();
		
		try {
			dao.create(trip);
			response.sendRedirect("Record.jsp?");
				
			

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

}
