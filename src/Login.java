
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Trip.Trip;
import dao.ApplicationDAOImpl;
import user.Admin;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Login() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try{
			// TODO Auto-generated method stub
			System.out.println("doGet metodu");
			// response.getWriter().append("Served at:
			// ").append(request.getContextPath());
			response.sendRedirect("home.jsp");
			doPost(request, response);
		}
		catch(IOException e){
			e.printStackTrace();
		}
		catch(ServletException e){
			e.printStackTrace();
		}
		

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		PrintWriter out = response.getWriter();
		System.out.println("doPost metodu");

		String username = request.getParameter("username");
		String password = request.getParameter("password");

		Admin admin = new Admin(username, password);

		ApplicationDAOImpl dao = new ApplicationDAOImpl();
		try {
			ResultSet rs = dao.selectOne(admin);
			
			

			if (rs.next()) {
				HttpSession session = request.getSession();
				session.setAttribute("name", username);
				if (username.equals("admin")) {
					response.sendRedirect("addAdmin.jsp?username=" + rs.getString("username"));
				} else
					response.sendRedirect("addTrip.jsp?username=" + rs.getString("username"));

			} else {
				out.println("Wrong username or password " + username + " " + password);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

}
